<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div style="width: 680px; margin: 0 auto">
		<table>
			<tbody>
				<tr>
					<td><img src="/MailSenderAddcel/resources/images/gdf/logodf.png"></td>
					<td><img src="/MailSenderAddcel/resources/images/gdf/logosefin.png"></td>
					<td><p style="font-size: 18px">Secretaría de Finanzas</p>
						<p style="font-size: 14px">Tesorería</p></td>
				</tr>
			</tbody>
		</table>
		<p style="font-weight: bold; font-size: 18px">Estimado
			Contribuyente.</p>
		<p>Usted ha realizado una transacción a través de la aplicación
			para teléfonos móviles de la Secretaría de Finanzas del Distrito
			Federal.</p>
		<p>Adjunto encontrará el recibo de pago correspondiente.</p>
		<p>Gracias por pagar oportunamente sus contribuciones.</p>
		<p>
			En la <span style="font-size: 18px; font-weight: bold;">Ciudad
				de México</span> estamos
		</p>
		<p style="font-size: 20px; color: #FFBF00; line-height: 2px">Decidiendo
			Juntos.</p>
		<div style="text-align: center">
			<p>Siguenos en</p>
			<img src="/MailSenderAddcel/resources/images/gdf/logofb.png"><img
				src="/MailSenderAddcel/resources/images/gdf/logotw.png"><img
				src="/MailSenderAddcel/resources/images/gdf/logoyt.png">
		</div>
		<p style="font-size: 12px">Nota. Este correo es de carácter
			informativo, no es necesario que responda al mismo.</p>
		<img src="/MailSenderAddcel/resources/images/gdf/footersefin.png">
	</div>
</body>
</html>