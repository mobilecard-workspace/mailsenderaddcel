<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Recibo Tenencia</title>
<style type="text/css">
@page {
	size: 21.59cm 27.94cm; /* width height */
	margin: 2cm 1.54cm 2cm 1.54cm;
}

#contenido {
	position: absolute;
	margin: 0 auto;
	width: 18.5cm;
	height: 20cm;
	font-family: serif;
}

.titulo-18pt {	
	font-size: 18pt;
	font-weight: bold;
}

.titulo-16pt {
	font-size: 16pt;
	font-weight: bold;	
}

.titulo-14pt {
	font-size: 14pt;
	font-weight: bold;	
}

.titulo-10pt {
	font-size: 10pt;
	width: 200px;
	font-weight: bold;
	margin-left: 0.5cm;
}

#encabezado {
	position: absolute;
	margin-top: 0px;
	height: 6cm;
	width: 100%;
}

#datos1 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
}

#datos2 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
	margin-left: 9.5cm;
}

#datos3 {
	position: absolute;
	height: 3cm;
	border: 1px solid;
	width: 100%;
	margin-top: 11.5cm;
}

#datos4 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 15cm;
	margin-left: 4.75cm;
	text-align: center;
}

#datos5 {
	position: absolute;
	height: 3cm;
	background-color: silver;
	width: 11cm;
	margin-top: 17cm;
}

#datos6 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 7cm;
	margin-top: 18.5cm;
	margin-left: 11.5cm;
	text-align: center;
}

.titulo {
	height: 0.5cm;
	background-color: rgb(40%, 40%, 40%);
	color: white;
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

.titulo-dos {
	height: 0.5cm;	
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

#logodf {
	position: absolute;
	height: 3.5cm;
	width: 3.5cm;
}

#logohsbc {
	height: 2cm;
	width: 4.5cm;
	float: right;
}

#texto1 {
	position: absolute;
	font-size: 10pt;
	font-weight: bold;
	margin-left: 4cm;
	margin-top: 1cm;
	width: 6cm;
}

#titulo-principal {
	position: absolute;
	margin-top: 3.5cm;
	text-align: center;
	width: 100%;
	font-weight: bold;
	border: 1px solid;	
}

#certificado-digital {
	font-size: 22pt;
	text-align: center;
	font-family: monospace;
	font-weight: bold;
}

#logo-escudo {
	float: right;
	margin-right: 5px;
	width: 1.4cm;
}

table.table-sin-borde {
	border-collapse: collapse;
	border: none;
	margin-left: 5px;
	font-weight: bold;	
	font-size: 10pt;
}

td.td-sin-borde {
	padding: 0;
}
</style>
</head>
<body>
	<div id="contenido">
		<div id="encabezado">
			<img id="logodf"
				src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoDfBn.png"
				alt="logodf" />
			<div id="texto1">SECRETARÍA DE FINANZAS TESORERÍA</div>
			<img id="logohsbc"
				src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoHSBC.jpeg"
				alt="logohsbc" />
			<div id="titulo-principal">
				<div><span class="titulo-18pt">RECIBO DE PAGO A LA TESORERÍA</span></div>
				<div><span class="titulo-18pt">Impuesto sobre Tenencia y Derechos</span></div>
				<div><span class="titulo-16pt">Realizado por Tesorería Móvil</span></div>				
			</div>
		</div>

		<div id="datos1">
			<div class="titulo">DATOS ADMINISTRATIVOS modelo</div>
			<table class="table-sin-borde">
				<tbody>
					<tr>
						<td class="td-sin-borde">Placa:</td>
						<td class="td-sin-borde">fgsgs</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Modelo:</td>
						<td class="td-sin-borde">343545</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="datos2">
			<div class="titulo">LIQUIDACIÓN DE PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<tr>
						<td class="td-sin-borde">Tenencia:</td>
						<td class="td-sin-borde">dato</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Act. Tenencia:</td>
						<td class="td-sin-borde">dato</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Rec. Tenencia:</td>
						<td class="td-sin-borde">datos</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Refrendo:</td>
						<td class="td-sin-borde">datos</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Act. Refrendo:</td>
						<td class="td-sin-borde">datos</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Rec. Refrendo:</td>
						<td class="td-sin-borde">datos</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="datos3">
			<div class="titulo">DATOS DEL PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<tr>
						<td class="td-sin-borde">Banco:</td>
						<td></td>
					</tr>
					<tr>
						<td class="td-sin-borde">No. de Autorización:</td>
						<td></td>
					</tr>
					<tr>
						<td class="td-sin-borde">Fecha de pago:</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="datos4">
			<div class="titulo">LÍNEA DE CAPTURA</div>
			<div class="titulo-14pt">230u9irwjoij4245</div>
		</div>
		<div id="datos5">
			<div class="titulo-dos">CERTIFICACIÓN DIGITAL DE LA TESORERÍA</div>
			<div id="certificado-digital">JADF42584892455TY76JJ</div>
			<img id="logo-escudo"
				src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoGdfEscudo.png"
				alt="logoescudo" />
		</div>
		<div id="datos6">
			<div class="titulo-dos">TOTAL PAGADO</div>
			<div style="text-decoration: underline;" class="titulo-14pt">$ 463456.00</div>
		</div>
	</div>	
</body>
</html>