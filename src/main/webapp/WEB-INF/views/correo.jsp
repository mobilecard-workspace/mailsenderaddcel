<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Insert title here</title>
</head>
<body>
	<table border=0 width=600>
		<tr>
			<td><p align=center>
					<font color=#333333 size=+2>Confirmaci&oacute;n de Compra </font>
				</p>
				<p align=center>
					<font color=#333333>Tu compra ha sido efectuada con
						&eacute;xito</font>
				</p>
				<p>
					<font color=#333333>Los datos de tu compra son:</font>
				</p>
				<p>
					<b><i>Fecha y Hora:</i></b> ${fecha} <br />
					<b><i>Cargo aplicado:</i></b>$ ${cargo}<br />
					<b><i>Autorizaci&oacute;n bancaria :</i></b> {autorizacion}
				</p>
				<p align=center>
					<font color=#333333>¡Gracias por comprar con MobileCARD!</font>
				</p>
				<p>
					<font color=#333333 size=-1>Para dudas y/o aclaraciones
						escribenos a <a href="mailto:soporte@addcel.com">soporte@addcel.com</a>
					</font>
				</p>
				<p>
					<font color=#333333 size=-1>Tambien puedes visitarnos en
						nuestra p&aacute;gina web: <a href="http://www.mobilecard.mx" target="_blank">www.mobilecard.mx</a>	
					</font>					
				</p></td>
		</tr>
	</table>
</body>
</html>