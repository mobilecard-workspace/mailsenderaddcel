<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Recibo Tenencia</title>
<style type="text/css">
@page {
	size: 21.59cm 27.94cm; /* width height */
	margin: 2cm 1.53cm 2cm 1.53cm;
}

#contenido {
	position: absolute;
	margin: 0 auto;
	width: 18.5cm;
	height: 20cm;
	font-family: serif;
}

.titulo-18pt {
	font-size: 18pt;
	font-weight: bold;
}

.titulo-16pt {
	font-size: 16pt;
	font-weight: bold;
}

.titulo-10pt {
	font-size: 10pt;
	width: 200px;
	font-weight: bold;
	margin-left: 0.5cm;
}

#encabezado {
	position: absolute;
	margin-top: 0px;
	height: 6cm;
	width: 100%;
}

#datos1 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
}

#datos2 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
	margin-left: 9.5cm;
}

#datos3 {
	position: absolute;
	height: 3cm;
	border: 1px solid;
	width: 100%;
	margin-top: 11.5cm;
}

#datos4 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 15cm;
	margin-left: 4.75cm;
	text-align: center;
}

#datos5 {
	position: absolute;
	height: 3cm;
	background-color: silver;
	width: 11cm;
	margin-top: 17cm;
}

#datos6 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 7cm;
	margin-top: 18.5cm;
	margin-left: 11.5cm;
	text-align: center;
}

.titulo {
	height: 0.5cm;
	background-color: rgb(40%, 40%, 40%);
	color: white;
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

#logodf {
	position: absolute;
	height: 3.5cm;
	width: 3.5cm;
}

#logohsbc {
	height: 2cm;
	width: 4.5cm;
	float: right;
}

#texto1 {
	position: absolute;
	font-size: 10pt;
	font-weight: bold;
	margin-left: 4cm;
	margin-top: 1cm;
	width: 6cm;
}

#titulo-principal {
	position: absolute;
	margin-top: 3.5cm;
	text-align: center;
	width: 100%;
	font-weight: bold;
}

#certificado-digital {
	font-size: 22pt;
	text-align: center;
	font-family: monospace;
	font-weight: bold;
}

#logo-escudo {
	float: right;
	margin-right: 5px;
	width: 1.4cm;
}

table.table-sin-borde {
	border-collapse: collapse;
	border: none;
	margin-left: 5px;
	font-weight: bold;
	font-size: 10pt;
}

.titulo-dos {
	height: 0.5cm;
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

.titulo-14pt {
	font-size: 14pt;
	font-weight: bold;
}

td.td-sin-borde {
	padding: 0;
}
</style>
</head>
<body>
	<div id="contenido">
		<div id="encabezado">
			<img id="logodf"
				src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoDfBn.png"
				alt="logodf" />
			<div id="texto1">SECRETARÍA DE FINANZAS TESORERÍA</div>
			<div id="header3">
				<span class="titulo-10pt">ADQUIRIENTE:</span>
				<img id="logohsbc"
					src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoHSBC.jpeg"
					alt="logohsbc" />
			</div>
			<div id="titulo-principal">
				<div>
					<span class="titulo-18pt">RECIBO DE PAGO A LA TESORERÍA templategdf</span>
				</div>
				<div>
					<span class="titulo-16pt">${obj.concepto}</span>
				</div>
				<div>
					<!-- span class="titulo-16pt">Realizado por Tesorería Móvil</span -->
				</div>
			</div>
		</div>
		<div id="datos1">
			<div class="titulo">DATOS ADMINISTRATIVOS</div>
			<table class="table-sin-borde">
				<tbody>
					<#-- Datos para del recibo de tenencia --> <#if obj.placa??>
					<tr>
						<td class="td-sin-borde">Placa:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.placa}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Modelo:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.modelo}</td>
					</tr>
					</#if> <#-- Datos para el recibo de infracción --> <#if
					obj.folio??>
					<tr>
						<td class="td-sin-borde">Folio:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.folio}</td>
					</tr>
					</#if> <#-- Datos para el recibo de nomina --> <#if obj.rfc??>
					<tr>
						<td class="td-sin-borde">RFC:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.rfc}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Periodo de Pago:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.periodoPago}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Tipo de Declaración:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.tipoDeclaracion}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">No. de Trabajadores:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.noTrabajadores}</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de predial --> <#if
					obj.cuentaPredial??>
					<tr>
						<td class="td-sin-borde">Cuenta Predial:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.cuentaPredial}</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de agua --> <#if
					obj.cuentaAgua??>
					<tr>
						<td class="td-sin-borde">Cuenta Agua:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.cuenta}</td>
					</tr>
					</#if>
				</tbody>
			</table>
		</div>
		<div id="datos2">
			<div class="titulo">LIQUIDACIÓN DE PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<#-- Datos para el recibo de tenencia --> <#-- Si existe una
					propiedad del tipo de recibo se asume que se trata de un recibo de
					ese tipo por lo que no se revisan los demas parametros. --> <#if
					obj.placa??>
					<tr>
						<td class="td-sin-borde">Tenencia:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.tenencia}</td>
					</tr>
					<#if obj.actTenencia?has_content>
					<tr>
						<td class="td-sin-borde">Act. Tenencia:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.actTenencia}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Rec. Tenencia:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.recTenencia}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Refrendo:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.refrendo}</td>
					</tr>
					<#if obj.actRefrendo?has_content>
					<tr>
						<td class="td-sin-borde">Act. Refrendo:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.actRefrendo}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Rec. Refrendo:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.recRefrendo}</td>
					</tr>
					</#if> <#-- Datos para el recibo de infracción --> <#if
					obj.folio??>
					<tr>
						<td class="td-sin-borde">Multa:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.multa}</td>
					</tr>
					<#if obj.actualizacion?has_content>
					<tr>
						<td class="td-sin-borde">Actualización:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.actualizacion}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Recargos:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.recargos}</td>
					</tr>
					</#if> <#-- Datos para el recibo de nomina --> <#if obj.rfc??>
					<tr>
						<td class="td-sin-borde">Rem Gravadas:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.remGravadas}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Impuesto:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.impuesto}</td>
					</tr>
					<#if obj.actualizacion?has_content>
					<tr>
						<td class="td-sin-borde">Actualización:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.actualizacion}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Recargos:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.recargos}</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de predial --> <#if
					obj.cuentaPredial??>
					<tr>
						<td class="td-sin-borde">Importe: </td>
						<td class="td-sin-borde">${obj.importe}</td>						
					</tr>
					<tr>
						<td class="td-sin-borde">Reducción: </td>
						<td class="td-sin-borde">${obj.reduccion}</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de agua --> <#if
					obj.cuentaAgua??>
					<tr>
						<td class="td-sin-borde">Importe: </td>
						<td class="td-sin-borde">${obj.importe}</td>						
					</tr>
					<tr>
						<td class="td-sin-borde">IVA: </td>
						<td class="td-sin-borde">${obj.iva}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Total a pagar:&nbsp&nbsp</td>
						<td class="td-sin-borde">${obj.total}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="datos3">
			<div class="titulo">DATOS DEL PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<tr>
						<td cliass="td-sin-borde">Banco:&nbsp&nbsp</td>
						<td>${obj.banco}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">No. de Autorización:&nbsp&nbsp</td>
						<td>${obj.numAutorizacion}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Fecha de pago:&nbsp&nbsp</td>
						<td>${obj.fechaPago}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="datos4">
			<div class="titulo">LÍNEA DE CAPTURA</div>
			<div class="titulo-14pt">${obj.linea_captura}</div>
		</div>
		<div id="datos5">
			<div class="titulo-dos">CERTIFICACIÓN DIGITAL DE LA TESORERÍA</div>
			<div id="certificado-digital">${obj.certificado}</div>
			<img id="logo-escudo"
				src="http://localhost:8084/MailSenderAddcel/resources/images/gdf/logoGdfEscudo.png"
				alt="logoescudo" />
		</div>
		<div id="datos6">
			<div class="titulo-dos">TOTAL PAGADO</div>
			<div style="text-decoration: underline;" class="titulo-14pt">$
				${obj.totalPago}</div>
		</div>
	</div>
</body>
</html>