package com.addcel.controller;

import java.io.FileOutputStream;


import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.addcel.model.dto.addcel.CorreoVO;
import com.addcel.model.dto.addcel.SmtpVO;
import com.addcel.model.dto.gdf.AguaVO;
import com.addcel.model.dto.gdf.InfraccionVO;
import com.addcel.model.dto.gdf.NominaVO;
import com.addcel.model.dto.gdf.PredialVO;
import com.addcel.model.dto.gdf.TenenciaVO;
import com.addcel.model.dto.gdf.PredialVencidoVO;
import com.addcel.services.MailSenderService;

import com.addcel.model.dto.tlajomulco.DetalleVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MailController {

	private static final Logger logger = LoggerFactory
			.getLogger(MailController.class);
	@Autowired
	private MailSenderService mailSenderService;

	/**
	 * Controller para el envio de correo general
	 * 
	 * @param correo
	 *            objeto json con todos los parametros necesarios para el envio
	 *            de correo
	 */
	@RequestMapping(value = "/enviaCorreoAddcel", consumes = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public void envioCorreoAddcel(@RequestBody CorreoVO correo) {
		mailSenderService.enviarCorreo(correo);
	}
	
	@RequestMapping(value = "/enviaCorreoAddcelTest", consumes = "application/json")
	public void envioCorreoAddcelTest(@RequestBody CorreoVO correo) {
		logger.info("Mail to: {}",  correo.getTo().toString());
		logger.info("Body: {}", correo.getBody());
		mailSenderService.enviarCorreo(correo);
	}

	/**
	 * Metodo para el envio de correo para la aplicacion PROTAGONISTAS DE
	 * NUESTRA TELE
	 * 
	 * @param idModulo
	 *            int modulo para tomar el contenido del correo desde la bd
	 * @param fecha
	 *            parametro para el cuerpo del correo
	 * @param cargo
	 *            parametro para el cuerpo del correo
	 * @param autorizacion
	 *            parametro para el cuerpo del correo
	 * @param correo
	 *            destinatario del correo
	 */
	@RequestMapping(value = "/mailprotagonistas/{idModulo}")
	@ResponseStatus(value = HttpStatus.OK)
	public void mailProtagonistas(
			@PathVariable("idModulo") int idModulo,
			@RequestParam(value = "fecha", required = false) String fecha,
			@RequestParam(value = "cargo", required = false) String cargo,
			@RequestParam(value = "autorizacion", required = false) String autorizacion,
			@RequestParam(value = "correo") String correo) {
		SmtpVO smtp = mailSenderService.obtenDatosSmtp(2, idModulo, 2);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("fecha", fecha);
		model.put("cargo", cargo);
		model.put("autorizacion", autorizacion);
		String[] correos = {correo};
		mailSenderService.enviarCorreo(correos, smtp, null, null, model);
	}

	/**
	 * 
	 * @param tipo
	 *            int tipo de servicio del cual se trate
	 * @param correo
	 *            email del destinatario
	 * @param json
	 *            string json con los datos para generar el recibo
	 */
	@RequestMapping(value = "/envia-recibo-gdf/{tipo}")
	@ResponseStatus(value = HttpStatus.OK)
	public void envioCorreoGdfRecibo(@PathVariable("tipo") int tipo,
			@RequestParam("correo") String correo,
			@RequestParam("json") String json) {
		SmtpVO smtp = mailSenderService.obtenDatosSmtp(3, 1, 16);
		byte[] pdf = null;
		Map<String, Object> pdfModel = new HashMap<String, Object>();
		logger.info("Tipo: " + tipo);
		logger.info("Correo: " + correo);
		logger.info("Json: " + json);
		logger.info("SMTP: " + smtp);
		try {
			if (tipo == 1) {// nomina
				NominaVO obj = new ObjectMapper().readValue(json, NominaVO.class);
				obj.setConcepto("Impuesto sobre Nominas");
				switch(Integer.parseInt(obj.getTipo_declaracion())){
					case 1:
							obj.setTipo_declaracion("Normal");
							break;
					case 2:
							obj.setTipo_declaracion("Complementaria");
							break;
					case 3:
							obj.setTipo_declaracion("Autocorrección");
							break;
					default:
							obj.setTipo_declaracion("N");
							break;
				}
				pdfModel.put("obj", obj);
				logger.info("generando recibo nomina");
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());				
			} else if (tipo == 2) {// tenencia
				TenenciaVO obj = new ObjectMapper().readValue(json, TenenciaVO.class);
				obj.setConcepto("Impuesto sobre Tenencia y Derechos");
				pdfModel.put("obj", obj);
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());
			} else if (tipo == 3) {// infraccion
				InfraccionVO obj = new ObjectMapper().readValue(json, InfraccionVO.class);
				obj.setConcepto("Infracciones de Transito");
				pdfModel.put("obj", obj);
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());
			} else if (tipo == 4) {// predial
				PredialVO obj = new ObjectMapper().readValue(json, PredialVO.class);
				obj.setConcepto("Impuesto Predial Vigente");
				pdfModel.put("obj", obj);
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());
			} else if (tipo == 5) {// Agua
				AguaVO obj = new ObjectMapper().readValue(json, AguaVO.class);
				logger.info("obj id bitacora: " + obj.getId_bitacora());
				obj.setConcepto("Derechos por Suministro de Agua Vigente");
				pdfModel.put("obj", obj);
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());
			} else if (tipo == 6){ //Predial Vencido
				PredialVencidoVO obj = new ObjectMapper().readValue(json, PredialVencidoVO.class);
				logger.info("obj id bitacora: " + obj.getId_bitacora());
				obj.setConcepto("Impuesto Predial Vencido");
				pdfModel.put("obj", obj);
				pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());				
			}
			logger.info("llamando a envio de correo");
			try{
				String patron = "yyyy-MM-dd_hhmmss";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				
				OutputStream out = new FileOutputStream("\\server\\PDF\\GDF_" + formato.format(new Date()) + ".pdf");
			     out.write(pdf);
			     out.close();        
			}catch(Exception e){
				logger.error("Error al guardar el PDF: ", e);
			}
//			
			String[] correos = {correo};
			
			mailSenderService.enviarCorreo(correos, smtp, pdf, "recibo-gdf",
					null);
			logger.info("fin llamado de correo");
		} catch (JsonParseException e) {
			logger.error("Error al parsear string json: ", e);
		} catch (JsonMappingException e) {
			logger.error("Error al mappear objeto json: ", e);
		} catch (IOException e) {
			logger.error("IOException: ", e);
		}

	}

	/**
	 * 
	 * @param tipo
	 *            int tipo de servicio del cual se trate
	 * @param correo
	 *            email del destinatario
	 * @param json
	 *            string json con los datos para generar el recibo
	 */
	@RequestMapping(value = "/envia-recibo-tlajomulco/")
	@ResponseStatus(value = HttpStatus.OK)
	public void envioCorreoTlajomulcoRecibo(@RequestParam("correos") String[] correos,
			@RequestParam("json") String json) {
		SmtpVO smtp = mailSenderService.obtenDatosSmtp(2, 1, 25);
		byte[] pdf = null;
		Map<String, Object> pdfModel = new HashMap<String, Object>();
		logger.info("Correos: " + correos);
		logger.info("Json: " + json);
		try {
			DetalleVO obj = new ObjectMapper().readValue(json, DetalleVO.class);
			String concepto = null;
			String labelimpuesto = null;
			switch(obj.getCuenta().trim().length()){
				case 8:
					concepto = "Impuesto Predial";
					labelimpuesto = "Impuesto: ";
					break;
				case 12:
					concepto = "Derechos por uso de Agua";
					labelimpuesto = "Derechos: ";
					break;
			}
			obj.setConcepto(concepto);
			obj.setLabelimpuesto(labelimpuesto);
			pdfModel.put("obj", obj);
			logger.info("generando recibo Tlajomulco por " + concepto);
			pdf = mailSenderService.generaPdf(pdfModel, smtp.getHtmlToPdf());				
			logger.info("llamando a envio de correo");
			try{
				String patron = "yyyy-MM-dd_hhmmss";
				SimpleDateFormat formato = new SimpleDateFormat(patron);
				
				OutputStream out = new FileOutputStream("\\server\\PDF\\Tlajomulco_" + formato.format(new Date()) + ".pdf");
			     out.write(pdf);
			     out.close();        
			}catch(Exception e){
				logger.error("Error al guardar el PDF: ", e);
			}
			mailSenderService.enviarCorreo(correos, smtp, pdf, "recibo-tlajomulco",	null);
			logger.info("fin llamado de correo");
		} catch (JsonParseException e) {
			logger.error("Error al parsear string json: ", e);
		} catch (JsonMappingException e) {
			logger.error("Error al mappear objeto json: ", e);
		} catch (IOException e) {
			logger.error("IOException: ", e);
		}

	}

	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Metodos que no se utilizan solo como control interno para pruebas
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		logger.info("Home controller");
		return "home";
	}

	@RequestMapping(value = "/templatemail", method = RequestMethod.GET)
	public String plantillaEmail() {
		return "template-mail";
	}

	@RequestMapping(value = "/templatepdf", method = RequestMethod.GET)
	public String plantillaPdf() {

		// return "correo";
		// return "pase_de_abordar";
		return "template-pdf";
	}

	@RequestMapping(value = "/correovo")
	public @ResponseBody
	CorreoVO correoVoJson() {
		CorreoVO correo = new CorreoVO();
		String[] to = { "gaidar26@gmail.com", "gaidar26@hotmail.com" };
		String[] bcc = { "jpmartinez@mindbits.com.mx", "gaidar26@gmail.com" };
		String[] urlfiles = { "C:\\Logs\\ProcomGDF.log",
				"C:\\Logs\\ProcomGDF.log", "C:\\Logs\\ProcomGDF.log" };
		correo.setBcc(bcc);
		correo.setCc(bcc);
		correo.setTo(to);
		correo.setBody("Saludos desde mobilecard");
		correo.setFrom("no-reply@addcel.com");
		correo.setSubject("asunto del correo");
		correo.setAttachments(urlfiles);
		// {"to":["correo1@correo","correo2@correo","correo3@correo","correo4@correo","correo4@correo"],"bcc":["correo1@correo","correo2@correo","correo3@correo","correo4@correo","correo4@correo"],"from":"no-reply@addcel.com","subject":"asunto del correo","body":"cuerpo","url":"url/archivos/files"}
		return correo;
	}

	/*
	 * @RequestMapping(value = "/nomina") public @ResponseBody NominaVO nomina()
	 * { NominaVO nomina=new NominaVO(); nomina.setId_producto("1");
	 * nomina.setBanco("banco"); nomina.setNo_autorizacion("444333");
	 * nomina.setFechaPago("2013/10/20");
	 * nomina.setCertificado("weeiu44345jinlk==");
	 * nomina.setTotal("123123.000)); nomina.setLinCaptu("jaksdfjcoraksdjf");
	 * 
	 * nomina.setRemuneraciones(new BigDecimal(11234.33));
	 * nomina.setImpuesto(new BigDecimal(16.331));
	 * nomina.setImpuesto_actualizado(new BigDecimal(23.9966));
	 * nomina.setRecargos(new BigDecimal(323.0033));
	 * 
	 * nomina.setRfc("pasdfe43453"); nomina.setAnio_pago("2013");
	 * nomina.setMes_pago("12"); nomina.setTipo_declaracion(4);
	 * nomina.setNum_trabajadores(12);
	 * 
	 * return nomina; }
	 * 
	 * @RequestMapping(value = "/tenencia") public @ResponseBody TenenciaVO
	 * tenecia() { TenenciaVO tenencia=new TenenciaVO();
	 * 
	 * tenencia.setId_producto("2"); tenencia.setBanco("banco");
	 * tenencia.setNo_autorizacion("444333");
	 * tenencia.setFechaPago("2013/10/20");
	 * tenencia.setCertificado("weeiu44345jinlk=="); tenencia.setTotal(new
	 * BigDecimal(123123.000)); tenencia.setLinCaptu("jaksdfjcoraksdjf");
	 * 
	 * tenencia.setPlaca("qrqrewr"); tenencia.setModelo(new BigDecimal(2134));
	 * tenencia.setEjercicio(new BigDecimal(234324));
	 * 
	 * tenencia.setTenencia(new BigDecimal(1212));
	 * tenencia.setTenActualizacion(new BigDecimal(134));
	 * tenencia.setTenRecargo(new BigDecimal(1324.00)); tenencia.setDerecho(new
	 * BigDecimal(123.99)); tenencia.setDerActualizacion(new
	 * BigDecimal(133.99)); tenencia.setDerRecargo(new BigDecimal(13.45));
	 * 
	 * 
	 * return tenencia; }
	 * 
	 * @RequestMapping(value = "/infraccion") public @ResponseBody InfraccionVO
	 * infraccion() { InfraccionVO infraccion=new InfraccionVO();
	 * 
	 * infraccion.setId_producto("3"); infraccion.setBanco("banco");
	 * infraccion.setNo_autorizacion("444333");
	 * infraccion.setFechaPago("2013/10/20");
	 * infraccion.setCertificado("weeiu44345jinlk==");
	 * infraccion.setTotal("123123.000");
	 * infraccion.setLinCaptu("jaksdfjcoraksdjf");
	 * 
	 * infraccion.setFolio("foiosdf");
	 * 
	 * infraccion.setDias_multa("dias multa");
	 * infraccion.setActualizacion("actualizacion");
	 * infraccion.setRecargos("recargos");
	 * 
	 * 
	 * return infraccion; }
	 * 
	 * @RequestMapping(value = "/predial") public @ResponseBody PredialVO
	 * predial() { PredialVO predial=new PredialVO();
	 * //--------------------------------------- predial.setId_producto("4");
	 * predial.setBanco("banco"); predial.setNo_autorizacion("444333");
	 * predial.setFechaPago("2013/10/20");
	 * predial.setCertificado("weeiu44345jinlk==");
	 * predial.setTotal("123123.000"); predial.setLinCaptu("jaksdfjcoraksdjf");
	 * 
	 * predial.setCuentaP("cuenta predial"); predial.setImporte("importe");
	 * predial.setReduccion("reduccion"); return predial; }
	 * 
	 * @RequestMapping(value = "/agua") public @ResponseBody AguaVO
	 * objectToJson() { AguaVO agua=new AguaVO();
	 * 
	 * agua.setId_producto("5"); agua.setBanco("banco");
	 * agua.setNo_autorizacion("444333"); agua.setFechaPago("2013/10/20");
	 * agua.setCertificado("weeiu44345jinlk=="); agua.setTotal("123123.000");
	 * agua.setLinCaptu("jaksdfjcoraksdjf");
	 * 
	 * agua.setCuenta("cuenta agua"); agua.setViva("iva");
	 * agua.setTotal("total"); //--------------------------------------- return
	 * agua; }
	 */

}
