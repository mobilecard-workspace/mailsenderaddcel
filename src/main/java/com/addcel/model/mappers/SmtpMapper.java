package com.addcel.model.mappers;

import org.apache.ibatis.annotations.Param;

import com.addcel.model.dto.addcel.SmtpVO;

public interface SmtpMapper {
	SmtpVO obtenDatosSmtp(@Param("idApp")int idApp,@Param("idModulo")int idModulo,@Param("idCliente")int idCliente);
}
