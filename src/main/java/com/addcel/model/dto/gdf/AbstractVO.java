package com.addcel.model.dto.gdf;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Formatter;

public class AbstractVO {
	private String id_bitacora;
	private String id_usuario;
	private int id_producto;
	private String no_autorizacion;
	private String linea_captura;
	private String fechaPago; 
	private String statusPago;
	private String error;
	private String descError;
	private String totalPago = "0.00";
	private String banco = "986";
	private String certificado ;
	private String concepto ;
	// MLS imprimir numero de tarjeta en pdf
	private String ultimos4TDC;

	
	private static final String patronImpCom = "###,###,##0.00";
	private static DecimalFormat formato;
	private static DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
	
	private static Formatter fmt= new Formatter();
	
	static{
		simbolos.setDecimalSeparator('.');
		simbolos.setGroupingSeparator(',');
		formato = new DecimalFormat(patronImpCom,simbolos);
	}
	
	
	public static String formatoImporte(String numString){
		String total = null;
		if(numString != null && !"".equals(numString)){
			total = formatoImporte(Double.parseDouble(numString));
		}
		return total;
	}
	
	public static String formatoImporte(double numDouble){
		String total = null;
		try{
			total = formato.format(numDouble);
		}catch(Exception e){
			
		}
		return total;
	}
	
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getDescError() {
		return descError;
	}
	public void setDescError(String descError) {
		this.descError = descError;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = formatoImporte(totalPago);
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	// inicio MLS imprimir numero de tarjeta en pdf
	public String getUltimos4TDC() {
		return ultimos4TDC;
	}

	public void setUltimos4TDC(String ultimos4tdc) {
		ultimos4TDC = ultimos4tdc;
	}	
	// fin MLS imprimir numero de tarjeta en pdf
		
}
