package com.addcel.model.dto.gdf;

public class InfraccionVO extends AbstractVO{
	private String placa;
    private String folio;
    private String importe;
    private String fechainfraccion;
    private String actualizacion;
    private String recargos;
    private String dias_multa;
    private int codeCalc;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = formatoImporte(importe);
	}
	public String getFechainfraccion() {
		return fechainfraccion;
	}
	public void setFechainfraccion(String fechainfraccion) {
		this.fechainfraccion = fechainfraccion;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = formatoImporte(actualizacion);
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = formatoImporte(recargos);
	}
	public String getDias_multa() {
		return dias_multa;
	}
	public void setDias_multa(String dias_multa) {
		this.dias_multa = dias_multa;
	}
	public int getCodeCalc() {
		return codeCalc;
	}
	public void setCodeCalc(int codeCalc) {
		this.codeCalc = codeCalc;
	}        
}
