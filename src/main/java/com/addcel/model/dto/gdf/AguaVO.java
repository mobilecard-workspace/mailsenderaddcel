/**
 * OutputDatosFormato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.model.dto.gdf;



public class AguaVO extends AbstractVO{
	private String vanio;
	private String vbimestre;
    private String vderdom;
    private String vderndom;
    private String vuso;
    private String viva;
    private String vtotal;
    private String cuenta;
    private String vigencia;
    // MLS Correccion de bug de Impuesto
    private String vderecho;
    
	public String getVanio() {
		return vanio;
	}
	public void setVanio(String vanio) {
		this.vanio = vanio;
	}
	public String getVbimestre() {
		return vbimestre;
	}
	public void setVbimestre(String vbimestre) {
		this.vbimestre = vbimestre;
	}
	public String getVderdom() {
		return vderdom;
	}
	public void setVderdom(String vderdom) {
		this.vderdom = vderdom;
	}
	public String getVderndom() {
		return vderndom;
	}
	public void setVderndom(String vderndom) {
		this.vderndom = vderndom;
	}
	public String getVuso() {
		return vuso;
	}
	public void setVuso(String vuso) {
		this.vuso = vuso;
	}
	public String getViva() {
		return viva;
	}
	public void setViva(String viva) {
		this.viva = formatoImporte(viva);
	}
	public String getVtotal() {
		return vtotal;
	}
	public void setVtotal(String vtotal) {
		this.vtotal = formatoImporte(vtotal);
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	// inicio MLS Correccion de bug de Impuesto
	public String getVderecho() {
		return vderecho;
	}
	public void setVderecho(String vderecho) {
		this.vderecho = vderecho;
	}
	// fin MLS Correccion de bug de Impuesto
}
