/**
 * Tipo_respuesta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.model.dto.gdf;

public class PredialVO  extends AbstractVO {
	private String bimestre;
    private String concepto;
    private String cuentaP;
    private String error_cel;
    private String error_descripcion;
    private String importe;
    private String intImpuesto;
    private String mensaje;
    private String reduccion;
    private String total;
    private String vencimiento;
	public String getBimestre() {
		return bimestre;
	}
	public void setBimestre(String bimestre) {
		this.bimestre = bimestre;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCuentaP() {
		return cuentaP;
	}
	public void setCuentaP(String cuentaP) {
		this.cuentaP = cuentaP;
	}
	public String getError_cel() {
		return error_cel;
	}
	public void setError_cel(String error_cel) {
		this.error_cel = error_cel;
	}
	public String getError_descripcion() {
		return error_descripcion;
	}
	public void setError_descripcion(String error_descripcion) {
		this.error_descripcion = error_descripcion;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = formatoImporte(importe);
	}
	public String getIntImpuesto() {
		return intImpuesto;
	}
	public void setIntImpuesto(String intImpuesto) {
		this.intImpuesto = intImpuesto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getReduccion() {
		return reduccion;
	}
	public void setReduccion(String reduccion) {
		this.reduccion = formatoImporte(reduccion);
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = formatoImporte(total);
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
}
