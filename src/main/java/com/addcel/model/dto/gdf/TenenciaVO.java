package com.addcel.model.dto.gdf;

public class TenenciaVO extends AbstractVO{
	private String placa;			//Placa del Vehiculo.
	private String condonacion;	//bandera para descuento por condonacion.
	private String interes;		//bandera para calcular interes.
	private String subsidio;		//bandera para otorgar subsidio.
	private String ejercicio;	//A�o de la tenencia que se quiere pagar.
	private String descrip; 		//Descripcion del error.
	private	String modelo;		//modelo del vehiculo que porta la placa.
	private String total;		//Importe final con la sumas y restas correspondientes.
	private String vigencia;			//Fecha de vigencia de la linea de captura.
	private String vigenciaD;			//Fecha de vigencia de la linea de captura.
	
	private String tipoServicio;
	private String valor_fact;
	private String cve_vehi;
	private String fech_factura;
	private String tipoCalculo;
	private String tenencia;
	private String descuento;
	private String derecho;
	private String tenActualizacion;
	private String tenRecargo;
	private String tenCondRecargo;
	private String totalTenencia;
	private String derActualizacion;
	private String derRecargo;
	private String totalDerecho;
	private String impuesto;
	private String derechos;
	private String actualizacion;
	private String recargos;
	private String condRecargos;
	private String intAdicional;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getCondonacion() {
		return condonacion;
	}
	public void setCondonacion(String condonacion) {
		this.condonacion = condonacion;
	}
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public String getSubsidio() {
		return subsidio;
	}
	public void setSubsidio(String subsidio) {
		this.subsidio = subsidio;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getVigenciaD() {
		return vigenciaD;
	}
	public void setVigenciaD(String vigenciaD) {
		this.vigenciaD = vigenciaD;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getValor_fact() {
		return valor_fact;
	}
	public void setValor_fact(String valor_fact) {
		this.valor_fact = valor_fact;
	}
	public String getCve_vehi() {
		return cve_vehi;
	}
	public void setCve_vehi(String cve_vehi) {
		this.cve_vehi = cve_vehi;
	}
	public String getFech_factura() {
		return fech_factura;
	}
	public void setFech_factura(String fech_factura) {
		this.fech_factura = fech_factura;
	}
	public String getTipoCalculo() {
		return tipoCalculo;
	}
	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}
	public String getTenencia() {
		return tenencia;
	}
	public void setTenencia(String tenencia) {
		this.tenencia = formatoImporte(tenencia);
	}
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	public String getDerecho() {
		return derecho;
	}
	public void setDerecho(String derecho) {
		this.derecho = formatoImporte(derecho);
	}
	public String getTenActualizacion() {
		return tenActualizacion;
	}
	public void setTenActualizacion(String tenActualizacion) {
		this.tenActualizacion = formatoImporte(tenActualizacion);
	}
	public String getTenRecargo() {
		return tenRecargo;
	}
	public void setTenRecargo(String tenRecargo) {
		this.tenRecargo = formatoImporte(tenRecargo);
	}
	public String getTenCondRecargo() {
		return tenCondRecargo;
	}
	public void setTenCondRecargo(String tenCondRecargo) {
		this.tenCondRecargo = tenCondRecargo;
	}
	public String getTotalTenencia() {
		return totalTenencia;
	}
	public void setTotalTenencia(String totalTenencia) {
		this.totalTenencia = totalTenencia;
	}
	public String getDerActualizacion() {
		return derActualizacion;
	}
	public void setDerActualizacion(String derActualizacion) {
		this.derActualizacion = formatoImporte(derActualizacion);
	}
	public String getDerRecargo() {
		return derRecargo;
	}
	public void setDerRecargo(String derRecargo) {
		this.derRecargo = formatoImporte(derRecargo);
	}
	public String getTotalDerecho() {
		return totalDerecho;
	}
	public void setTotalDerecho(String totalDerecho) {
		this.totalDerecho = totalDerecho;
	}
	public String getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}
	public String getDerechos() {
		return derechos;
	}
	public void setDerechos(String derechos) {
		this.derechos = formatoImporte(derechos);
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getCondRecargos() {
		return condRecargos;
	}
	public void setCondRecargos(String condRecargos) {
		this.condRecargos = condRecargos;
	}
	public String getIntAdicional() {
		return intAdicional;
	}
	public void setIntAdicional(String intAdicional) {
		this.intAdicional = intAdicional;
	}
	public String getTenSubsidio() {
		return tenSubsidio;
	}
	public void setTenSubsidio(String tenSubsidio) {
		this.tenSubsidio = tenSubsidio;
	}
	private String tenSubsidio;
}
