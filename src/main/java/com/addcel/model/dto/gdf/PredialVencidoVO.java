/**
 * Tipo_respuesta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.model.dto.gdf;

import java.math.BigDecimal;
import java.util.List;


public class PredialVencidoVO  extends AbstractVO {
	private int idError;
	private String mensajeError;
    private String cadbc;
    private String cond_gastos_ejecucion;
    private String cond_multa_incumplimiento;
    private String cuenta;
    private String interes;
    private List <PredialVencidoDetalleVO> detalle;
    private BigDecimal gastos_ejecucion;
    private BigDecimal multa_incumplimiento;
    private String total_cuenta;
    private String bimestres;
    private String concepto;
    private BigDecimal sumActualizacion;
    private BigDecimal sumRecargos;
    private BigDecimal sumMultas;
    private BigDecimal impuestoEmitido;
    private BigDecimal impuestoPagado;
	
    public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getCadbc() {
		return cadbc;
	}
	public void setCadbc(String cadbc) {
		this.cadbc = cadbc;
	}
	public String getCond_gastos_ejecucion() {
		return cond_gastos_ejecucion;
	}
	public void setCond_gastos_ejecucion(String cond_gastos_ejecucion) {
		this.cond_gastos_ejecucion = cond_gastos_ejecucion;
	}
	public String getCond_multa_incumplimiento() {
		return cond_multa_incumplimiento;
	}
	public void setCond_multa_incumplimiento(String cond_multa_incumplimiento) {
		this.cond_multa_incumplimiento = cond_multa_incumplimiento;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public List<PredialVencidoDetalleVO> getDetalle() {
		return detalle;
	}
	public void setDetalle(List<PredialVencidoDetalleVO> detalle) {
		this.detalle = detalle;
	}
	public BigDecimal getGastos_ejecucion() {
		return gastos_ejecucion;
	}
	public void setGastos_ejecucion(BigDecimal gastos_ejecucion) {
		this.gastos_ejecucion = gastos_ejecucion;
	}
	public BigDecimal getMulta_incumplimiento() {
		return multa_incumplimiento;
	}
	public void setMulta_incumplimiento(BigDecimal multa_incumplimiento) {
		this.multa_incumplimiento = multa_incumplimiento;
	}
	public String getTotal_cuenta() {
		return total_cuenta;
	}
	public void setTotal_cuenta(String total_cuenta) {
		this.total_cuenta = total_cuenta;
	}
	public String getBimestres() {
		return bimestres;
	}
	public void setBimestres(String bimestres) {
		this.bimestres = bimestres;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public BigDecimal getSumActualizacion() {
		return sumActualizacion;
	}
	public void setSumActualizacion(BigDecimal sumActualizacion) {
		this.sumActualizacion = sumActualizacion;
	}
	public BigDecimal getSumRecargos() {
		return sumRecargos;
	}
	public void setSumRecargos(BigDecimal sumRecargos) {
		this.sumRecargos = sumRecargos;
	}
	public BigDecimal getSumMultas() {
		return sumMultas;
	}
	public void setSumMultas(BigDecimal sumMultas) {
		this.sumMultas = sumMultas;
	}
	public BigDecimal getImpuestoEmitido() {
		return impuestoEmitido;
	}
	public void setImpuestoEmitido(BigDecimal impuestoEmitido) {
		this.impuestoEmitido = impuestoEmitido;
	}
	public BigDecimal getImpuestoPagado() {
		return impuestoPagado;
	}
	public void setImpuestoPagado(BigDecimal impuestoPagado) {
		this.impuestoPagado = impuestoPagado;
	}

}
