package com.addcel.model.dto.gdf;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class NominaVO extends AbstractVO{

	private String clave;
	private String rfc;
	private String mes_pago;
	private String anio_pago;
	
	private String remuneraciones;
	private String impuesto;
	private String impuesto_actualizado;
	private String recargos;
	private String recargos_condonado;
	private String interes;		
	private String Total;
	
	private String vigencia;
	private String lineacapturaCB;
	private String error_descripcion;
	
	private String tipo_declaracion;
	private String num_trabajadores;
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getMes_pago() {
		return mes_pago;
	}
	public void setMes_pago(String mes_pago) {
		this.mes_pago = mes_pago;
	}
	public String getAnio_pago() {
		return anio_pago;
	}
	public void setAnio_pago(String anio_pago) {
		this.anio_pago = anio_pago;
	}
	public String getRemuneraciones() {
		return remuneraciones;
	}
	public void setRemuneraciones(String remuneraciones) {
		this.remuneraciones = formatoImporte(remuneraciones);
	}
	public String getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(String impuesto) {
		this.impuesto = formatoImporte(impuesto);
	}
	public String getImpuesto_actualizado() {
		return impuesto_actualizado;
	}
	public void setImpuesto_actualizado(String impuesto_actualizado) {
		this.impuesto_actualizado = formatoImporte(impuesto_actualizado);
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = formatoImporte(recargos);
	}
	public String getRecargos_condonado() {
		return recargos_condonado;
	}
	public void setRecargos_condonado(String recargos_condonado) {
		this.recargos_condonado = formatoImporte(recargos_condonado);
	}
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getLineacapturaCB() {
		return lineacapturaCB;
	}
	public void setLineacapturaCB(String lineacapturaCB) {
		this.lineacapturaCB = lineacapturaCB;
	}
	public String getError_descripcion() {
		return error_descripcion;
	}
	public void setError_descripcion(String error_descripcion) {
		this.error_descripcion = error_descripcion;
	}
	public String getTipo_declaracion() {
		return tipo_declaracion;
	}
	public void setTipo_declaracion(String tipo_declaracion) {
		this.tipo_declaracion = tipo_declaracion;
	}
	public String getNum_trabajadores() {
		return num_trabajadores;
	}
	public void setNum_trabajadores(String num_trabajadores) {
		this.num_trabajadores = num_trabajadores;
	}
}
