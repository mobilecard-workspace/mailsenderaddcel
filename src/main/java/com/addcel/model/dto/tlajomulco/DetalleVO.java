package com.addcel.model.dto.tlajomulco;

import java.math.BigDecimal;

public class DetalleVO {
	private String referencia;
	private String nombre;
	private Long id_bitacora;
	private String id_usuario;
	private String fecha;
	private int status;
	private String email;
	private BigDecimal comision;
	private BigDecimal total_pago;
	private Long id_record; 
	private String recaudadora;
	private String tipo_predio;
	private String cuenta;
	private String clave_catastral;
	private String subpredio;
	private String propietario;
	private String calle_fiscal;
	private String noexterior_fiscal;
	private String interior_fiscal;
	private String colonia_fiscal;
	private String poblacion_fiscal;
	private String calle;
	private String num_exterior;
	private String num_interior;
	private String colonia;
	private BigDecimal impuesto;
	private BigDecimal recargo;
	private BigDecimal desc_recargos;
	private BigDecimal recargo_adeuda;               
	private BigDecimal gastos;
	private BigDecimal multas; 	
	private BigDecimal desc_multa;
	private BigDecimal multa_adeuda;
	private BigDecimal saldo;
	private int axoini;
	private int bimini;
	private String concepto;
	private String labelimpuesto;
	
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(Long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BigDecimal getComision() {
		return comision;
	}
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
	public BigDecimal getTotal_pago() {
		return total_pago;
	}
	public void setTotal_pago(BigDecimal total_pago) {
		this.total_pago = total_pago;
	}
	public Long getId_record() {
		return id_record;
	}
	public void setId_record(Long id_record) {
		this.id_record = id_record;
	}
	public String getRecaudadora() {
		return recaudadora;
	}
	public void setRecaudadora(String recaudadora) {
		this.recaudadora = recaudadora;
	}
	public String getTipo_predio() {
		return tipo_predio;
	}
	public void setTipo_predio(String tipo_predio) {
		this.tipo_predio = tipo_predio;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getClave_catastral() {
		return clave_catastral;
	}
	public void setClave_catastral(String clave_catastral) {
		this.clave_catastral = clave_catastral;
	}
	public String getSubpredio() {
		return subpredio;
	}
	public void setSubpredio(String subpredio) {
		this.subpredio = subpredio;
	}
	public String getPropietario() {
		return propietario;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}
	public String getCalle_fiscal() {
		return calle_fiscal;
	}
	public void setCalle_fiscal(String calle_fiscal) {
		this.calle_fiscal = calle_fiscal;
	}
	public String getNoexterior_fiscal() {
		return noexterior_fiscal;
	}
	public void setNoexterior_fiscal(String noexterior_fiscal) {
		this.noexterior_fiscal = noexterior_fiscal;
	}
	public String getInterior_fiscal() {
		return interior_fiscal;
	}
	public void setInterior_fiscal(String interior_fiscal) {
		this.interior_fiscal = interior_fiscal;
	}
	public String getColonia_fiscal() {
		return colonia_fiscal;
	}
	public void setColonia_fiscal(String colonia_fiscal) {
		this.colonia_fiscal = colonia_fiscal;
	}
	public String getPoblacion_fiscal() {
		return poblacion_fiscal;
	}
	public void setPoblacion_fiscal(String poblacion_fiscal) {
		this.poblacion_fiscal = poblacion_fiscal;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNum_exterior() {
		return num_exterior;
	}
	public void setNum_exterior(String num_exterior) {
		this.num_exterior = num_exterior;
	}
	public String getNum_interior() {
		return num_interior;
	}
	public void setNum_interior(String num_interior) {
		this.num_interior = num_interior;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public BigDecimal getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}
	public BigDecimal getRecargo() {
		return recargo;
	}
	public void setRecargo(BigDecimal recargo) {
		this.recargo = recargo;
	}
	public BigDecimal getDesc_recargos() {
		return desc_recargos;
	}
	public void setDesc_recargos(BigDecimal desc_recargos) {
		this.desc_recargos = desc_recargos;
	}
	public BigDecimal getRecargo_adeuda() {
		return recargo_adeuda;
	}
	public void setRecargo_adeuda(BigDecimal recargo_adeuda) {
		this.recargo_adeuda = recargo_adeuda;
	}
	public BigDecimal getGastos() {
		return gastos;
	}
	public void setGastos(BigDecimal gastos) {
		this.gastos = gastos;
	}
	public BigDecimal getMultas() {
		return multas;
	}
	public void setMultas(BigDecimal multas) {
		this.multas = multas;
	}
	public BigDecimal getDesc_multa() {
		return desc_multa;
	}
	public void setDesc_multa(BigDecimal desc_multa) {
		this.desc_multa = desc_multa;
	}
	public BigDecimal getMulta_adeuda() {
		return multa_adeuda;
	}
	public void setMulta_adeuda(BigDecimal multa_adeuda) {
		this.multa_adeuda = multa_adeuda;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public int getAxoini() {
		return axoini;
	}
	public void setAxoini(int axoini) {
		this.axoini = axoini;
	}
	public int getBimini() {
		return bimini;
	}
	public void setBimini(int bimini) {
		this.bimini = bimini;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getLabelimpuesto() {
		return labelimpuesto;
	}
	public void setLabelimpuesto(String labelimpuesto) {
		this.labelimpuesto = labelimpuesto;
	}

}
