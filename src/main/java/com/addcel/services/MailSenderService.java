package com.addcel.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.addcel.model.dto.addcel.CorreoVO;
import com.addcel.model.dto.addcel.SmtpVO;
import com.addcel.model.mappers.SmtpMapper;
import com.itextpdf.text.DocumentException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class MailSenderService {
	private static final Logger logger = LoggerFactory
			.getLogger(MailSenderService.class);
	@Autowired
	private SmtpMapper mapper;		
	@Autowired
	private JavaMailSenderImpl mailSender;
	@Autowired
	private Configuration freeMarkerEngine;

	/**
	 * Metodo que obtiene la informaci�n referente al envio de correo
	 * de la aplicaci�n
	 * @param idApp id de la aplicacion
	 * @param idModulo id del modulo
	 * @param idCliente id del cliente
	 * @return
	 */
	public SmtpVO obtenDatosSmtp(int idApp,int idModulo,int idCliente){
		return mapper.obtenDatosSmtp(idApp,idModulo,idCliente);
	}
	/**
	 * Metodo para el envio de correo
	 * 
	 * @param to
	 *            destinatario del correo
	 * @param smtp
	 *            datos de configuracion del smtp
	 * @param pdf
	 *            array de byte del pdf a adjuntar
	 * @param nombrePdf
	 *            nombre del pdf
	 * @param model
	 *            Map con los datos para rellenar <br>
	 *            la plantilla html que sera el cuerpo del correo
	 */
	@Async
	public void enviarCorreo(String[] to, SmtpVO smtp, byte[] pdf,
			String nombrePdf, Map<String, Object> model) {		
		if (to != null) {
			// plantilla para el env�o de email
			logger.info("smtp: "+smtp.getSmtpServer());
			logger.info("user: "+smtp.getUsuario());
			logger.info("cliente: "+smtp.getIdCliente());
			logger.info("pwd: "+smtp.getPassword());
			final MimeMessage message = mailSender.createMimeMessage();
			try {
				// el flag a true indica que va a ser multipart
				final MimeMessageHelper helper = new MimeMessageHelper(message,
						true, "UTF-8");
				// settings de los par�metros del env�o
				helper.setTo(to);
				helper.setSubject(smtp.getSubject());
				helper.setFrom(smtp.getCorreo());
				if (smtp.getBcc() != null) {
					helper.setBcc(smtp.getBcc());
				}

				Template t = new Template("plantilla", new StringReader(smtp.getBody()), freeMarkerEngine);
				String bodyHtml = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
				logger.info("==>>>> set body");
				helper.setText(bodyHtml, true);
				if (pdf != null) {
					helper.addAttachment(nombrePdf, new ByteArrayDataSource(
							pdf, "application/pdf"));
				} else {
					logger.info("No se envio ningun pdf para adjuntar");
				}
				mailSender.send(message);
				logger.info("Correo enviado exitosamente a: {}", to);
			} catch (IOException e) {
				logger.error("Error al procesar la plantilla: {}", e);
			} catch (TemplateException e) {
				logger.error("Error al procesar la plantilla: {}", e);
			} catch (MessagingException e) {
				logger.error("Error al enviar email", e);
			}catch (Exception e) {				
				logger.error("===> Error {}", e.getMessage());
			}			
		} else {
			logger.error("No se recibío ningún destinatario");
		}
	}

	@Async
	public void enviarCorreo(CorreoVO correo) {
		if (correo.getTo() != null) {
			// plantilla para el env�o de email
			final MimeMessage message = mailSender.createMimeMessage();
			FileSystemResource res = null;
			try {
				// el flag a true indica que va a ser multipart
				final MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
				helper.setTo(correo.getTo());
				helper.setSubject(correo.getSubject());
				if(correo.getFrom() == null){
					helper.setFrom("no-reply@addcel.com");
				}else{
					helper.setFrom(correo.getFrom());
				}
				if (correo.getBcc() != null) {
					helper.setBcc(correo.getBcc());
				}
				if(correo.getCc() != null){
					helper.setCc(correo.getCc());
				}
				helper.setText(correo.getBody(), true);
				if(correo.getCid() != null){
					try{
						for(int i = 0 ; i < correo.getCid().length; i++){
							logger.info("CID: identifierCID" + (i<10? "0" +i: i) + ":"+ correo.getCid()[i]);
							res = new FileSystemResource(new File(correo.getCid()[i]));
							helper.addInline("identifierCID" + (i<10? "0" +i: i), res);
						}
					}catch(Exception e){
						logger.error("Ocurrio un error al adjuntar el CID", e);
					}
					
				} else {
					logger.info("No se tiene ningun CID");
				}
				
				if (correo.getAttachments() != null) {
					try{
						for (String urlFile : correo.getAttachments()) {
							logger.info("File: "+ urlFile);
							res = new FileSystemResource(urlFile);
							helper.addAttachment(res.getFilename(), res);
						}
					}catch(Exception e){
						logger.error("Ocurrio un error al adjuntar un archivo", e);
					}
					
				} else {
					logger.info("No se tiene ningun archivo para adjuntar");
				}
				mailSender.send(message);
				logger.info("Correo enviado exitosamente");
			} catch (Exception e) {
				logger.error("Error al enviar email", e);
			}			
		} else {
			logger.error("No se recib�o ning�n destinatario");
		}

	}

	/**
	 * M�todo para generar un pdf a partir de una plantilla html
	 * 
	 * @param modelo
	 *            Map con los datos para llenar la plantilla html
	 * @param htmlToPdf
	 *            string que representa la plantilla que se convertira en pdf
	 * @return array de byte del pdf generado
	 */
	public byte[] generaPdf(Map<String, Object> modelo, String htmlToPdf) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String pdfHtml = "";
		try {
			Template t = new Template("plantilla", new StringReader(htmlToPdf), freeMarkerEngine);
			pdfHtml = FreeMarkerTemplateUtils.processTemplateIntoString(t, modelo);
		} catch (IOException e) {
			logger.error("Error al procesar la plantilla: {}", e);
		} catch (TemplateException e) {
			logger.error("Error al procesar la plantilla: {}", e);
		}

		ITextRenderer renderer = new ITextRenderer();
		try {
			renderer.setDocumentFromString(pdfHtml);
			renderer.layout();
			renderer.createPDF(baos);
		} catch (DocumentException e) {
			logger.error("Error al procesar el Ducument: {}", e);
		} catch (IOException e) {
			logger.error("Error IO : {}", e);
		}

		return baos.toByteArray();
	}

	/**
	 * M�todo para generar un pdf a partir de una plantilla ftl ubicada en
	 * WEB-INF/templates/nombreplantilla.ftl
	 * 
	 * @param modelo
	 *            Map con los datos para llenar la plantilla ftl
	 * @param htmlToPdf
	 *            string que representa la plantilla que se convertira en pdf
	 * @return array de byte del pdf generado
	 */
	public byte[] generaPdfPlantillaFtl(Map<String, Object> modelo,
			String htmlToPdf) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String pdfHtml = "";
		try {
			pdfHtml = FreeMarkerTemplateUtils.processTemplateIntoString(
					freeMarkerEngine.getTemplate("templategdf.ftl"), modelo);
		} catch (IOException e) {
			logger.error("Error al procesar la plantilla: {}", e);
		} catch (TemplateException e) {
			logger.error("Error al procesar la plantilla: {}", e);
		}

		ITextRenderer renderer = new ITextRenderer();
		try {
			renderer.setDocumentFromString(pdfHtml);
			renderer.layout();
			renderer.createPDF(baos);
		} catch (DocumentException e) {
			logger.error("Error al procesar el Ducument: {}", e);
		} catch (IOException e) {
			logger.error("Error IO : {}", e);
		}

		return baos.toByteArray();
	}
}
